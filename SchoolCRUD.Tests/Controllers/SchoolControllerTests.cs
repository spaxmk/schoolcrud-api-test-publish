﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SchoolCRUD.Controllers;
using SchoolCRUD.Models;
using SchoolCRUD.Repository.Interfaces;

namespace SchoolCRUD.Tests.Controllers
{
    [TestClass]
    public class SchoolControllerTests
    {
        [TestMethod]
        public void GetReturnsSchoolWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<ISchoolRepository>();
            mockRepository.Setup(x => x.GetById(50)).Returns(new School { Id = 50 , Ime = "Unit Test Skola" , Adresa = "Unit Test Adresa", Mesto = "Novi Sad", Kontakt = "0123456789", GodinaRada = 2000 });

            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetSchool(50);
            var contentResult = actionResult as OkNegotiatedContentResult<School>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(50, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<ISchoolRepository>();
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetSchool(50);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<ISchoolRepository>();
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<ISchoolRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new School { Id = 50, Ime = "Unit Test Skola", Adresa = "Unit Test Adresa", Mesto = "Novi Sad", Kontakt = "0123456789", GodinaRada = 2000 });
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<ISchoolRepository>();
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PutSchool( new School { Ime = "Unit Test Skola" }, 10 );

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<ISchoolRepository>();
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PostSchool(new School { Id = 50, Ime = "Unit Test Skola", Adresa = "Unit Test Adresa", Mesto = "Novi Sad", Kontakt = "0123456789", GodinaRada = 2000 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<School>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(50, createdResult.RouteValues["id"]);
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<SchoolDTO> schools = new List<SchoolDTO>();
            schools.Add(new SchoolDTO { Id = 1, Ime = "Unit Test Skola 1", Mesto = "Novi Sad" });
            schools.Add(new SchoolDTO { Id = 2, Ime = "Unit Test Skola 1", Mesto = "Novi Sad" });

            var mockRepository = new Mock<ISchoolRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(schools.AsEnumerable());
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IEnumerable<SchoolDTO> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(schools.Count, result.ToList().Count);
            Assert.AreEqual(schools.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(schools.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void GetReturnsMultipleDetailsObjects()
        {
            // Arrange
            List<SchoolDetailDTO> schools = new List<SchoolDetailDTO>();
            schools.Add(new SchoolDetailDTO { Id = 1, Ime = "Unit Test Skola", Adresa = "Unit Test Adresa", Mesto = "Novi Sad", Kontakt = "0123456789", GodinaRada = 2000 });
            schools.Add(new SchoolDetailDTO { Id = 2, Ime = "Unit Test Skola", Adresa = "Unit Test Adresa", Mesto = "Novi Sad", Kontakt = "0123456789", GodinaRada = 2000 });

            var mockRepository = new Mock<ISchoolRepository>();
            mockRepository.Setup(x => x.GetAllDetail()).Returns(schools.AsEnumerable());
            var controller = new SchoolsController(mockRepository.Object);

            // Act
            IEnumerable<SchoolDetailDTO> result = controller.GetAllDetail();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(schools.Count, result.ToList().Count);
            Assert.AreEqual(schools.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(schools.ElementAt(1), result.ElementAt(1));
        }

    }
}
