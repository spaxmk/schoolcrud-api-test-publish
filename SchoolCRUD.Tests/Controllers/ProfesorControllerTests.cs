﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SchoolCRUD.Controllers;
using SchoolCRUD.Models;
using SchoolCRUD.Repository.Interfaces;

namespace SchoolCRUD.Tests
{
    [TestClass]
    public class ProfesorControllerTest
    {
        [TestMethod]
        public void GetReturnsProfesorsWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IProfesorRepository>();
            mockRepository.Setup(x => x.GetById(50)).Returns(new Profesor { Id = 50, Zvanje = Zvanje.dr, Ime = "Petar", Prezime = "Peric", Adresa = "Kumanovska 17", Grad = "Beograd", Kontakt = "0601234567", SchoolId = 1 });

            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetProfesor(50);
            var contentResult = actionResult as OkNegotiatedContentResult<Profesor>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(50, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IProfesorRepository>();
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetProfesor(50);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IProfesorRepository>();
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IProfesorRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Profesor { Id = 50, Zvanje = Zvanje.dr, Ime = "Petar", Prezime = "Peric", Adresa = "Kumanovska 17", Grad = "Beograd", Kontakt = "0601234567", SchoolId = 1 });
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IProfesorRepository>();
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PutProfesor(new Profesor { Ime = "Mara", Prezime = "Maric" }, 10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IProfesorRepository>();
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PostProfesor(new Profesor { Id = 50, Zvanje = Zvanje.dr, Ime = "Petar", Prezime = "Peric", Adresa = "Kumanovska 17", Grad = "Beograd", Kontakt = "0601234567", SchoolId = 1 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Profesor>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(50, createdResult.RouteValues["id"]);
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<ProfesorDTO> profesor = new List<ProfesorDTO>();
            profesor.Add(new ProfesorDTO { Id = 1, Ime = "Nikola", Prezime = "Nikolic" });
            profesor.Add(new ProfesorDTO { Id = 2, Ime = "Dimitrije", Prezime = "Dimitrijevic" });

            var mockRepository = new Mock<IProfesorRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(profesor.AsEnumerable());
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IEnumerable<ProfesorDTO> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(profesor.Count, result.ToList().Count);
            Assert.AreEqual(profesor.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(profesor.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void GetReturnsMultipleDetailsObjects()
        {
            // Arrange
            List<ProfesorDetailDTO> profesor = new List<ProfesorDetailDTO>();
            profesor.Add(new ProfesorDetailDTO { Id = 50, Zvanje = Zvanje.dr, Ime = "Petar", Prezime = "Peric", Adresa = "Kumanovska 17", Grad = "Beograd", Kontakt = "0601234567" });
            profesor.Add(new ProfesorDetailDTO { Id = 50, Zvanje = Zvanje.dr, Ime = "Petar", Prezime = "Peric", Adresa = "Kumanovska 17", Grad = "Beograd", Kontakt = "0601234567" });

            var mockRepository = new Mock<IProfesorRepository>();
            mockRepository.Setup(x => x.GetAllDetail()).Returns(profesor.AsEnumerable());
            var controller = new ProfesorsController(mockRepository.Object);

            // Act
            IEnumerable<ProfesorDetailDTO> result = controller.GetAllDetail();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(profesor.Count, result.ToList().Count);
            Assert.AreEqual(profesor.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(profesor.ElementAt(1), result.ElementAt(1));
        }
    }
}
