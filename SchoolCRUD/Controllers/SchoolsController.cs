﻿using SchoolCRUD.Models;
using SchoolCRUD.Repository;
using SchoolCRUD.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace SchoolCRUD.Controllers
{
    public class SchoolsController : ApiController
    {
        ISchoolRepository _repository { get; set; }

        public SchoolsController(ISchoolRepository repository)
        {
            _repository = repository;
        }

        //GET All schools with DTO (Id, Ime, Mesto)
        [HttpGet]
        [ResponseType(typeof(SchoolDTO))]
        [Route("api/schools")]
        public IEnumerable<SchoolDTO> GetAll()
        {
            return _repository.GetAll();
        }

        //GET All schools with Detail DTO (//Id, Ime, Mesto, GodinaRada, Kontakt, Adresa)
        [HttpGet]
        [ResponseType(typeof(SchoolDetailDTO))]
        [Route("api/schools/details")]
        public IEnumerable<SchoolDetailDTO> GetAllDetail()
        {
            return _repository.GetAllDetail();
        }

        //GET Schools by Id
        [HttpGet]
        [ResponseType(typeof(SchoolDetailDTO))]
        [Route("api/schools/{Id}")]
        public IHttpActionResult GetSchool(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var school = _repository.GetById(id);

            if (school == null)
                return NotFound();

            return Ok(school);
        }

        //POST School
        [HttpPost]
        [ResponseType(typeof(School))]
        [Route("api/schools")]
        public IHttpActionResult PostSchool(School school)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            _repository.Add(school);

            return CreatedAtRoute("DefaultApi", new { id = school.Id }, school);
        }

        // PUT Schools
        [HttpPut]
        [ResponseType(typeof(School))]
        [Route("api/schools/{id}")]
        public IHttpActionResult PutSchool(School school, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (id != school.Id)
                return BadRequest();
            try
            {
                _repository.Update(school);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(school);
        }

        //DELETE Schools
        [ResponseType(typeof(School))]
        [Route("api/schools/{Id}")]
        public IHttpActionResult Delete(int id)
        {
            var school = _repository.GetById(id);
            if (school == null)
                return NotFound();
            _repository.Delete(school);
            return Ok();
        }
    }
}
