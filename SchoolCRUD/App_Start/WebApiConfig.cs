﻿using SchoolCRUD.Repository;
using SchoolCRUD.Repository.Interfaces;
using SchoolCRUD.Resolver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace SchoolCRUD
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity
            var container = new UnityContainer();
            container.RegisterType<ISchoolRepository, SchoolRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IProfesorRepository, ProfesorRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            //var container2 = new UnityContainer();
            //container2.RegisterType<IProfesorRepository, ProfesorRepository>(new HierarchicalLifetimeManager());
            ////container.RegisterType<IProfesorRepository, ProfesorRepository>(new HierarchicalLifetimeManager());
            //config.DependencyResolver = new UnityResolver(container2);
        }
    }
}
