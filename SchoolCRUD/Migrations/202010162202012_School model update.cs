﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Schoolmodelupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.School", "Kontakt", c => c.Int(nullable: false));
            AddColumn("dbo.School", "Adresa", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.School", "Adresa");
            DropColumn("dbo.School", "Kontakt");
        }
    }
}
