﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updateprofesor : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Profesor", "Kontakt", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Profesor", "Kontakt", c => c.Int(nullable: false));
        }
    }
}
