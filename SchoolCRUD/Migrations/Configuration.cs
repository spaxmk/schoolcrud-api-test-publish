﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Principal;

    internal sealed class Configuration : DbMigrationsConfiguration<SchoolCRUD.Models.SchoolDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SchoolCRUD.Models.SchoolDbContext context)
        {
            context.Schools.AddOrUpdate(
                new Models.School() { Id = 1, Ime = "Sveti Sava", Mesto = "Sremska Mitrovica", GodinaRada = 2000, Adresa = "Bajci Zilinskog 13", Kontakt = "02111134" },
                new Models.School() { Id = 2, Ime = "Branko Radicevic", Mesto = "Sid", GodinaRada = 1999, Adresa = "Rumenacka 1", Kontakt = "021333545" }
                );
            context.SaveChanges();
        }
    }
}
