﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initProff : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Profesor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Zvanje = c.Int(nullable: false),
                        Ime = c.String(nullable: false, maxLength: 20),
                        Prezime = c.String(nullable: false, maxLength: 20),
                        Kontakt = c.Int(nullable: false),
                        Adresa = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                        SchoolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.School", t => t.SchoolId, cascadeDelete: true)
                .Index(t => t.SchoolId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profesor", "SchoolId", "dbo.School");
            DropIndex("dbo.Profesor", new[] { "SchoolId" });
            DropTable("dbo.Profesor");
        }
    }
}
