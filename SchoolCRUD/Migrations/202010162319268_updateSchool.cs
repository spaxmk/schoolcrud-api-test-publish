﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateSchool : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.School", "Kontakt", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.School", "Adresa", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.School", "Adresa", c => c.String(nullable: false));
            AlterColumn("dbo.School", "Kontakt", c => c.Int(nullable: false));
        }
    }
}
