﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DTO : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.School", "Ime", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.School", "Mesto", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.School", "Mesto", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.School", "Ime", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
