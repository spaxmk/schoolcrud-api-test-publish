﻿namespace SchoolCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updatetable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Schools", newName: "School");
            AlterColumn("dbo.School", "Ime", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.School", "Mesto", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.School", "Mesto", c => c.String());
            AlterColumn("dbo.School", "Ime", c => c.String());
            RenameTable(name: "dbo.School", newName: "Schools");
        }
    }
}
