﻿$(document).ready(function () {
    var host = window.location.host

    //$("#load").hide()
    $("#btnSchools").click(function () {
        $("#divDetails").toggle()

        $.ajax({
            //beforeSend: function () {
            //    $("#load").show()
            //},
            "type": "GET",
            "url": "https://" + host + "/api/schools/"
        })
            .done(function (data, status) {
                var $container = $("#data")
                $container.empty()
                $("#load").hide()

                if (status == "success") {
                    console.log(data)

                    var div = $("<div class='col-md-8'></div>")
                    var table = $("<table class='table table-border' id='tableAction'> \
                                  </table><br />")
                    var tableHeader = $("<thead>\
                                         <tr>\
                                         <th>ID</th>\
                                         <th>Name</th>\
                                         <th>Place</th>\
                                         </tr>\
                                         </thead>")
                    table.append(tableHeader)
                    for (i = 0; i < data.length; i++) {
                        var row = "<tr>"
                        var display = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td><td>" + data[i].Mesto + "</td>"
                        row += display + "</tr>"
                        table.append(row)
                    }
                    div.append(table)
                    $container.append(div)
                    $("#tableAction").on("click", "tr td", function () {
                        var cval = $(this).text()

                        $.ajax({
                            "type": "GET",
                            "url": "https://" + host + "/api/schools/" + cval + "/"
                        })
                            .done(function (data, status) {
                                getAll(data, status)
                            })
                            .fail(function (xhr) {
                                alert(xhr.responseText)
                            })
                    })
                }
            })
            .fail(function (xhr) {
                alert(xhr.responseText)
            })
    })
    function getAll(data, status) {
        var $containerDetails = $("#dataDetails")
        $containerDetails.empty()

        if (status == "success") {
            console.log(data)

            var $whiteSpace = ''
            var div = $("<div class='col-md-6'></div>")
            var dl = $("\
                        <ul class='list-group'>\
                        <li class='list-group-item active'><b><i>Details</i></b></li>\
                        <li class='list-group-item'><b>Name:</b> "+ $whiteSpace + "" + data.Ime + "</li>\
                        <li class='list-group-item'><b>Address:</b> "+ $whiteSpace + "" + data.Adresa + "</li>\
                        <li class='list-group-item'><b>City:</b> "+ $whiteSpace + "" + data.Mesto + "</li>\
                        <li class='list-group-item'><b>Contact:</b> "+ $whiteSpace + "" + data.Kontakt + "</li>\
                        <li class='list-group-item'><b>Year:</b> "+ $whiteSpace + "" + data.GodinaRada + "</li>\
                        </ul>")
            div.append(dl)
            $containerDetails.append(div)
        }
    }
})