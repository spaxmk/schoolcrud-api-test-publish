﻿using SchoolCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolCRUD.Repository.Interfaces
{
    public interface ISchoolRepository
    {
        IEnumerable<SchoolDTO> GetAll();
        IEnumerable<SchoolDetailDTO> GetAllDetail();
        School GetById(int? id);
        void Add(School school);
        void Update(School school);
        void Delete(School school);
    }
}
