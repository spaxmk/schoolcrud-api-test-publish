﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SchoolCRUD.Models
{
    public class ProfesorDetailDTO
    {
        public int Id { get; set; }
        public Zvanje Zvanje { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Kontakt { get; set; }
        public string Adresa { get; set; }
        public string Grad { get; set; }
        public string School { get; set; }
    }
}