﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolCRUD.Models
{
    public class SchoolDetailDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Mesto { get; set; }
        public string Kontakt { get; set; }
        public int GodinaRada { get; set; }
    }
}