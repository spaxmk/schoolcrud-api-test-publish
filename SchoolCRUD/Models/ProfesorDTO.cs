﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolCRUD.Models
{
    public class ProfesorDTO
    {
        public int Id { get; set; }
        public Zvanje Zvanje { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
    }
}