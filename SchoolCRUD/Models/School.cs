﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolCRUD.Models
{
    [Table("School")]
    public class School
    {
        
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Ime { get; set; }
        [Required]
        [StringLength(50)]
        public string Mesto { get; set; }
        [Required]
        public int GodinaRada { get; set; }
        [Required]
        [StringLength(15)]
        public string Kontakt { get; set; }
        [Required]
        [StringLength(50)]
        public string Adresa { get; set; }
    }
}