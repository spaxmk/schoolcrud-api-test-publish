﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SchoolCRUD.Models
{
    public class SchoolDbContext : DbContext
    {
        public SchoolDbContext() : base("name=SchoolDbContext")
        {

        }
     
        public DbSet<School> Schools { get; set; }
        public DbSet<Profesor> Profesors { get; set; }
    }
}